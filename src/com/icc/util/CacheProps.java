package com.icc.util;

import java.util.Properties;

public class CacheProps extends Properties {
	private static final long serialVersionUID = 1L;
	private static CacheProps me = null;
	
	public static CacheProps getInstance() {
		if ( me == null ) {
			me = new CacheProps();
		}
		
		return me;
	}
}
