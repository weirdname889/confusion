package com.icc.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utils {
	
	private static SimpleDateFormat format = null;
	
	public static SimpleDateFormat getFormat() {
		if ( format == null ) {
			format = new SimpleDateFormat( Constants.CREATED_TIME_FORMAT );
		    format.setTimeZone(TimeZone.getTimeZone("UTC"));
		}
		
		return format;
	}
	
	public static java.util.Date convertStringToUtilDate( String date ) throws ParseException {
		Date returnVal = getFormat().parse( date );
		return returnVal;
	}
	
	public static String convertUtilDateToString( Date date ) {
		return getFormat().format( date );
	}
	
	/**
	 * Returns date object that is 59 seconds before the supplied date
	 * @param date
	 * @return
	 */
	public static java.util.Date getSixtySecondsExclusiveDate( Date date ) {
		Calendar cal = Calendar.getInstance();
	    cal.setTime( date );
	    cal.add( Calendar.SECOND, -59 );
	    Date sixtySecondsExclusiveDate = cal.getTime();
	    
	    return sixtySecondsExclusiveDate;
	}

}
