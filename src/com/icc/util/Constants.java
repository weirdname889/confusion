package com.icc.util;

public class Constants {

	// JSON Constants
	public static final String CREATED_TIME = "created_time";
	public static final String ACTOR = "actor";
	public static final String TARGET = "target";
	public static final String CREATED_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	
	// Database Constants
	public static final String DB_PARAM_STRING = "string";
	public static final String DB_PARAM_DATETIME = "datetime";
}
