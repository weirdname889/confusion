package com.icc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBHelper {
	
	// Logger
	private static Logger logger = Logger.getLogger( DBHelper.class.getName() );
	
	public static Connection getConnection() throws Exception {

		String driverClass = CacheProps.getInstance().getProperty( "driverclassname" );
		if ( driverClass == null || "".equals( driverClass.trim() )) {
			throw new Exception( "driverclassname property is missing or empty ");
		}
		Class.forName(  driverClass );

		String dburl = CacheProps.getInstance().getProperty( "dburl" );
		if ( dburl == null || "".equals( dburl.trim() )) {
			throw new Exception( "dburl property is missing or empty ");
		}
		String dbuser = CacheProps.getInstance().getProperty( "dbuser" );
		if ( dbuser == null || "".equals( dbuser.trim() )) {
			throw new Exception( "dbuser property is missing or empty ");
		}
		String dbpassword = CacheProps.getInstance().getProperty( "dbpassword" );
		if ( dbpassword == null || "".equals( dbpassword.trim() )) {
			throw new Exception( "dbpassword property is missing or empty ");
		}
		Connection con = DriverManager.getConnection( dburl, dbuser, dbpassword );
		return con;
	}

	public static void executeUpdate( Connection con, String sql, 
			ArrayList<SQLParam> params ) throws Exception {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement( sql );
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( sql );
			}
			setParams( ps, params );
			long starttime = System.currentTimeMillis();
			ps.executeUpdate();
			long endtime = System.currentTimeMillis();
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( "Execution time (ms): " + (endtime-starttime) );
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ex2) {
				logger.log( Level.SEVERE, "Could not close preparedstatement", ex2 );
			}
		}
	}
	
	public static String getSingleValueFromTimestamp( Connection con, String sql,
			ArrayList<SQLParam> params ) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String returnVal = null;
		try { 
			ps = con.prepareStatement (sql );
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( sql );
			}
			setParams( ps, params );
			long starttime = System.currentTimeMillis();
			rs = ps.executeQuery();
			long endtime = System.currentTimeMillis();
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( "Execution time (ms): " + (endtime-starttime) );
			}
			if ( rs.next() ) {
				Timestamp ts = rs.getTimestamp(1);
				Date date = new Date( ts.getTime() );
				returnVal = Utils.convertUtilDateToString( date );
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.log( Level.SEVERE, "Could not close resultset", e );
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					logger.log( Level.SEVERE, "Could not close preparedstatement", e );
				}
			}
		}
		
		return returnVal;
	}
	
	private static void setParams( PreparedStatement ps, ArrayList<SQLParam> params ) throws Exception {
		if ( params != null && params.size() > 0 && ps != null ) {
			SQLParam sqlParam = null;
			for ( int i = 0; i<params.size(); i++ ) {
				sqlParam = params.get(i);
				if ( sqlParam.getType() == Constants.DB_PARAM_STRING ) {
					if ( logger.isLoggable( Level.FINE ) ) {
						logger.fine( "Param " + (i+1) + " [STRING]: " + sqlParam.getValue() );
					}
					ps.setString((i+1), sqlParam.getValue());
				} else if ( sqlParam.getType() == Constants.DB_PARAM_DATETIME ) {
					if ( logger.isLoggable( Level.FINE ) ) {
						logger.fine( "Param " + (i+1) + " [DATETIME]: " + sqlParam.getValue() );
					}
					ps.setTimestamp((i+1), convertToSQLTimestamp( sqlParam.getValue() ));
				}
			}
		}
	}
	
	private static Timestamp convertToSQLTimestamp( String date ) throws Exception {
		Date utilDate = Utils.convertStringToUtilDate( date );
		Timestamp ts = new Timestamp( utilDate.getTime() );
		return ts;
	}
	
	public static ArrayList<Integer> getIntegers( Connection con, String sql ) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Integer> returnVal = new ArrayList<Integer>();
		try { 
			ps = con.prepareStatement( sql );
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( sql );
			}
			long starttime = System.currentTimeMillis();
			rs = ps.executeQuery();
			long endtime = System.currentTimeMillis();
			if ( logger.isLoggable( Level.FINE ) ) {
				logger.fine( "Execution time (ms): " + (endtime-starttime) );
			}
			while ( rs.next() ) {
				returnVal.add( rs.getInt( 1 ) );
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.log( Level.SEVERE, "Could not close resultset", e );
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					logger.log( Level.SEVERE, "Could not close preparedstatement", e );
				}
			}
		}
		
		return returnVal;
	}
}
