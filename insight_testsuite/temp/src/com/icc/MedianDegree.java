package com.icc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.icc.util.CacheProps;
import com.icc.util.Constants;
import com.icc.util.DBHelper;
import com.icc.util.SQLParam;
import com.icc.util.Utils;

public class MedianDegree {

	// The maximum date processed in the file so far
	private static Date maxDate = null;
	// The current rolling median 
	private static double currentMedian = 0.00f;
	// Database connection
	private static Connection con = null;
	// Buffered reader for venmo file
	private static BufferedReader reader = null;
	// JSON Parser
	private static JsonParser jsonParser = new JsonParser();
	// JSON Elements
	private static JsonElement jsonRootElement = null;
	private static JsonElement jsonElement = null;
	// JSON Object
	private static JsonObject jsonObject = null;
	// Record information
	private static String createdTime = null;
	private static String actor = null;
	private static String target = null;
	// Writer for output file
	private static FileWriter writer = null;
	// Count of lines processed in input file
	private static int totalLineCount = 0;
	// Logger
	private static Logger logger = Logger.getLogger( MedianDegree.class.getName() );
	// StringBuffer/String for checking duplicate attributes
	private static StringBuffer duplicateChkBuf = new StringBuffer( "" );
	private static String duplicateChkStr = null;

	/**
	 * @param args
	 */
	public static void main( String[] args ) {
		
		// Verify path to properties file passed in as argument
		if ( args.length != 1 ){
			System.err.println("Error: path to properties file must be supplied as "
					+ "command-line argument.  Exiting ...");
			System.exit(1);
		}
		
		System.out.println( "Commencing processing ... ");
		long starttime = System.currentTimeMillis();

		try {
			// Load properties
			CacheProps.getInstance().load( new FileReader( args[0] ) );
			
			// Set up logging
			String logfileloc = CacheProps.getInstance().getProperty( "logfileloc" );
			if ( logfileloc == null || "".equals( logfileloc.trim() )) {
				throw new Exception( "logfileloc property is missing or empty");
			}
			String loglevel = CacheProps.getInstance().getProperty( "loglevel" );
			if ( loglevel == null || "".equals( loglevel.trim() )) {
				// Default to SEVERE
				loglevel = "SEVERE";
			}
			Logger root = Logger.getLogger( "" );
			LogManager.getLogManager().reset();
			FileHandler fileHandler = new FileHandler( logfileloc );
			fileHandler.setFormatter( new SimpleFormatter() );
			root.addHandler( fileHandler );
			root.setLevel( Level.parse( loglevel ) );
			
			// Establish database connection
			con = DBHelper.getConnection();
			
			// Create output file
			String outputfileloc = CacheProps.getInstance().getProperty( "outputfileloc" );
			if ( outputfileloc == null || "".equals( outputfileloc.trim() )) {
				throw new Exception( "outputfileloc property is missing or empty");
			}
			writer = new FileWriter( outputfileloc );
			
			// Clear existing records from DB (if any)
			clearExistingRecords();
			
			// Iterate over all venmo entries from text file
			String inputfileloc = CacheProps.getInstance().getProperty( "inputfileloc" );
			if ( inputfileloc == null || "".equals( inputfileloc.trim() )) {
				throw new Exception( "inputfileloc property is missing or empty");
			}
			reader = new BufferedReader( new FileReader( inputfileloc ) );
			String line = null;
			while ( (line = reader.readLine()) != null ) {
				// Process each transaction serially
				processRecord( line );
				totalLineCount++;
			}
			
			long endtime = System.currentTimeMillis();
			StringBuffer msgBuf = new StringBuffer( "Finished processing. Processed " );
			msgBuf.append( totalLineCount );
			msgBuf.append( " lines in " );
			msgBuf.append( (endtime-starttime) );
			msgBuf.append( " msec." );
			System.out.println( msgBuf.toString() );
			logger.info( msgBuf.toString() );
			
		} catch (Exception ex) {
			System.err.println("Processing finished with errors. See log file.");
			logger.log( Level.SEVERE, "Fatal error during processing", ex );
		}  finally {
			// Clean up resources
			if ( reader != null ) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.log( Level.SEVERE, "Could not close reader", e );
				}
			}
			if ( con != null ) {
				try {
					con.close();
				} catch (SQLException e) {
					logger.log( Level.SEVERE, "Could not close connection", e );
				}
			}
			if ( writer != null ) {
				try {
					writer.close();
				} catch (Exception e) {
					logger.log( Level.SEVERE, "Could not close file writer", e );
				}
			}
		}
	}
	
	private static void processRecord( String line ) {
		// First ensure none of the attributes actor, target, created_time are duplicated
		duplicateChkBuf.append( "\"" );
		duplicateChkBuf.append( Constants.ACTOR );
		duplicateChkBuf.append( "\"" );
		duplicateChkStr = duplicateChkBuf.toString();
		int firstIndex = line.indexOf( duplicateChkStr );
		int secondIndex = line.indexOf( duplicateChkStr, firstIndex+1 );
		if ( secondIndex != -1 ) {
			// Don't output median in case of ill-formatted line
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Duplicated actor attribute in line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
		}
		duplicateChkBuf.delete( 0, duplicateChkBuf.length() );
		duplicateChkBuf.append( "\"" );
		duplicateChkBuf.append( Constants.TARGET );
		duplicateChkBuf.append( "\"" );
		duplicateChkStr = duplicateChkBuf.toString();
		firstIndex = line.indexOf( duplicateChkStr );
		secondIndex = line.indexOf( duplicateChkStr, firstIndex+1 );
		if ( secondIndex != -1 ) {
			// Don't output median in case of ill-formatted line
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Duplicated target attribute in line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
		}
		duplicateChkBuf.delete( 0, duplicateChkBuf.length() );
		duplicateChkBuf.append( "\"" );
		duplicateChkBuf.append( Constants.CREATED_TIME );
		duplicateChkBuf.append( "\"" );
		duplicateChkStr = duplicateChkBuf.toString();
		firstIndex = line.indexOf( duplicateChkStr );
		secondIndex = line.indexOf( duplicateChkStr, firstIndex+1 );
		if ( secondIndex != -1 ) {
			// Don't output median in case of ill-formatted line
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Duplicated created_time attribute in line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
		}
		
		// Parse line into JSON object
	    try {
	    	jsonRootElement = jsonParser.parse( line );
			jsonObject = jsonRootElement.getAsJsonObject();
		} catch (Exception e) {
			// Don't output median in case of ill-formatted line
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Could not parse root JSON element for line: " +
						(totalLineCount+1), e );
			}
			resetLocalVariables();
			return;
		}
	    
	    // Ensure each line has only three attributes
	    if ( jsonObject.size() > 3 ) {
	    	// Don't output median in case of ill-formatted line
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "More than three attributes exists for line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
	    }
	    
	    // Extract created_time, actor, target and check for nulls
	    jsonElement = jsonObject.get( Constants.CREATED_TIME );
	    if ( jsonElement == null || "".equals( jsonElement.getAsString() ) ) {
	    	// Don't output median in case of missing created_time value
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Created_time value missing for line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
	    } else {
	    	createdTime = jsonElement.getAsString().trim();
	    }
	    jsonElement = jsonObject.get( Constants.ACTOR );
	    if ( jsonElement == null || "".equals( jsonElement.getAsString() ) ) {
	    	// Don't output median in case of missing created_time value
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "actor value missing for line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
	    } else {
	    	actor = jsonElement.getAsString().trim();
	    }
	    jsonElement = jsonObject.get( Constants.TARGET );
	    if ( jsonElement == null || "".equals( jsonElement.getAsString() ) ) {
	    	// Don't output median in case of missing created_time value
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "target value missing for line: " +
						(totalLineCount+1) );
			}
			resetLocalVariables();
			return;
	    } else {
	    	target = jsonElement.getAsString().trim();
	    }
	    
	    // Update the maximum date seen as necessary
	    Date createdTimeDate = null;
	    try {
			createdTimeDate = Utils.convertStringToUtilDate( createdTime );
		} catch (ParseException e) {
			// Don't output median in case of unparseable created_time value
			if ( logger.isLoggable( Level.WARNING ) ) {
				logger.log( Level.WARNING, "Could not parse created_time value for line: " +
						(totalLineCount+1), e );
			}
			resetLocalVariables();
			return;
		}
	    
	    if ( maxDate == null ) {
	    	// This is first date we've seen, so set maxdate
	    	maxDate = new Date( createdTimeDate.getTime() );
	    } else if ( isOutsideWindow( createdTimeDate ) ) {
	    	// Current record is 60 or more seconds earlier than maximum date, so ignore
	    	if ( logger.isLoggable( Level.WARNING ) ) {
	    		StringBuffer buf = new StringBuffer("The date: ");
	    		buf.append(createdTime);
	    		buf.append(" for line: ");
	    		buf.append((totalLineCount+1));
	    		buf.append(" is before window");
				logger.log( Level.WARNING, buf.toString() );
			}
	    	outputMedian();
	    	resetLocalVariables();
	    	return;
	    } else if ( createdTimeDate.after( maxDate ) ) {
	    	// New maximum date found - update variable 
	    	maxDate = new Date( createdTimeDate.getTime() );
	    	
	    	// Purge existing records that fall outside the new window
	    	purgeOldRecords();
	    }

	    Date existingDate = getExistingDateTimeFromDB();
	    if ( existingDate == null ) {
	    	// Insert records into database - two for each entry
		    insertDBRecords();
	    } else {
	    	// If the new date is later than the existing, update the timestamp.  This is to
	    	// prevent adding duplicate pair into DB for same edge list
	    	if ( createdTimeDate.after( existingDate ) ) {
	    		updateDBRecordDateTime();
	    	}
	    	
	    	// No need to recalculate median
	    	outputMedian();
	    	resetLocalVariables();
	    	return;
	    }
	    
	    // Recalculate median
	    try {
			recalculateMedian();
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error recalculating median for line: " + (totalLineCount+1), e );
		}
	    
	    // Reset local variables for reuse
	    resetLocalVariables();
	    
	    // Write the current median to file
	    outputMedian();
	}
	
	private static void insertDBRecords() {
		StringBuffer buf = new StringBuffer( "INSERT INTO records (actor, target, " );
		buf.append( "created_time) VALUES (?,?,?)" );
		String sql = buf.toString();
		ArrayList<SQLParam> params = new ArrayList<SQLParam>();
		// First Parameter
		SQLParam sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( actor );
		params.add( sqlParam );
		// Second Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( target );
		params.add( sqlParam );
		// Third Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_DATETIME );
		sqlParam.setValue( createdTime );
		params.add( sqlParam );
		
		// Execute SQL
		try {
			DBHelper.executeUpdate( con, sql, params );
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error inserting DB records for line: " + (totalLineCount+1), e );
		}
		
		// Execute second SQL for reverse entry, only if actor and target are different
		if ( !actor.equals(target) ) {
			sql = "INSERT INTO records (target, actor, created_time) VALUES (?,?,?)";
			try {
				DBHelper.executeUpdate( con, sql, params );
			} catch (Exception e) {
				logger.log( Level.SEVERE, "Error inserting DB records for line: " + (totalLineCount+1), e );
			}
		}
	}
	
	private static void outputMedian()  {
		try {
			if ( totalLineCount == 0 ) {
				writer.write( String.format("%.2f", currentMedian) );
			} else {
				writer.write( "\n" + String.format("%.2f", currentMedian) );
			}
		} catch (IOException e) {
			logger.log( Level.SEVERE, "Error outputting median for line: " + (totalLineCount+1), e );
		}
	}
	
	private static boolean isOutsideWindow( Date createdTimeDate ) {
		Date sixtySecondsExclusiveDate = Utils.getSixtySecondsExclusiveDate( maxDate );
	    return createdTimeDate.before( sixtySecondsExclusiveDate );
	}
	
	private static void clearExistingRecords() {
		String sql = "DELETE FROM records";
		// Execute SQL
		try {
			DBHelper.executeUpdate( con, sql, null );
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error clearing existing records", e );
		}
	}
	
	private static void purgeOldRecords() {
		Date sixySecondsExclusiveDate = Utils.getSixtySecondsExclusiveDate( maxDate );
		String sixySecondsExclusiveDateStr = Utils.convertUtilDateToString(
				sixySecondsExclusiveDate );
		String sql = "DELETE FROM records WHERE created_time < ?";
		// Parameter
		ArrayList<SQLParam> params = new ArrayList<SQLParam>();
		SQLParam sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_DATETIME );
		sqlParam.setValue( sixySecondsExclusiveDateStr );
		params.add( sqlParam );
		
		// Execute SQL
		try {
			DBHelper.executeUpdate( con, sql, params );
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error purging old records for line: " + (totalLineCount+1), e );
		}
	}
	
	private static Date getExistingDateTimeFromDB() {
		StringBuffer buf = new StringBuffer( "SELECT created_time FROM records WHERE " );
		buf.append( "((actor = ? AND target = ?) OR (actor = ? AND target = ?))" );
		String sql = buf.toString();
		ArrayList<SQLParam> params = new ArrayList<SQLParam>();
		// First Parameter
		SQLParam sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( actor );
		params.add( sqlParam );
		// Second Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( target );
		params.add( sqlParam );
		// Third Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( target );
		params.add( sqlParam );
		// Fourth Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( actor );
		params.add( sqlParam );
		
		// Execute SQL
		Date returnVal = null;
		try {
			String dateStr = DBHelper.getSingleValueFromTimestamp( con, sql, params );
			if ( dateStr != null ) {
				returnVal = Utils.convertStringToUtilDate( dateStr );
			}
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error getting datetime from db for line: " 
						+ (totalLineCount+1), e );
		}
		
		return returnVal;
	}
	
	private static void updateDBRecordDateTime() {
		StringBuffer buf = new StringBuffer( "UPDATE records SET created_time = ? WHERE " );
		buf.append( "((actor = ? AND target = ?) OR (actor = ? AND target = ?))" );
		String sql = buf.toString();
		ArrayList<SQLParam> params = new ArrayList<SQLParam>();
		// First Parameter
		SQLParam sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_DATETIME );
		sqlParam.setValue( createdTime );
		params.add( sqlParam );
		// Second Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( actor );
		params.add( sqlParam );
		// Third Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( target );
		params.add( sqlParam );
		// Fourth Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( target );
		params.add( sqlParam );
		// Fifth Parameter
		sqlParam = new SQLParam();
		sqlParam.setType( Constants.DB_PARAM_STRING );
		sqlParam.setValue( actor );
		params.add( sqlParam );
		
		// Execute SQL
		try {
			DBHelper.executeUpdate( con, sql, params );
		} catch (Exception e) {
			logger.log( Level.SEVERE, "Error updating db timestamp for line: " + (totalLineCount+1), e );
		}
	}
	
	private static void recalculateMedian() throws Exception {
		// Get total count of vertices in graph
		StringBuffer buf = new StringBuffer( "SELECT COUNT(1) FROM (SELECT COUNT(actor) " );
		buf.append( "FROM records GROUP BY actor) a" );
		String sql = buf.toString();
		int vertexCount = DBHelper.getIntegers(con, sql).get( 0 ).intValue();
		
		// If the vertex count is even, get the two middle edge degrees and average them, otherwise
		// get the middle edge degree
		if ( vertexCount % 2 == 0 ) {
			// Average middle values
			buf.delete( 0, buf.length() );
			buf.append( "SELECT COUNT(actor) FROM records GROUP BY actor " );
			buf.append( "ORDER BY COUNT(actor) LIMIT " );
			buf.append( (vertexCount/2)-1 );
			buf.append( ",2" );
			sql = buf.toString();
			ArrayList<Integer> middleValuesList = DBHelper.getIntegers( con, sql );
			currentMedian = ((double)middleValuesList.get( 0 ).intValue() +
					(double)middleValuesList.get( 1 ).intValue())/(double)2;
			
		} else {
			// Select middle value
			buf.delete( 0, buf.length() );
			buf.append( "SELECT COUNT(actor) FROM records GROUP BY actor " );
			buf.append( "ORDER BY COUNT(actor) LIMIT " );
			buf.append( (int)(vertexCount/2) );
			buf.append( ",1" );
			sql = buf.toString();
			currentMedian = (double)DBHelper.getIntegers( con, sql ).get( 0 ).intValue();
		}
	}
	
	private static void resetLocalVariables() {
		jsonRootElement = null;
	    jsonObject = null;
	    jsonElement = null;
	    createdTime = null;
	    actor = null;
	    target = null;
	    duplicateChkBuf.delete( 0, duplicateChkBuf.length() );
	    duplicateChkStr = null;
	}
 }
